unit HadddockException;

interface

uses
	SysUtils;

type
	EHaddockException = class( Exception )
	private
		function CaptianHaddockSays : string;

	public
		constructor Create( const Msg : string ); reintroduce;
		constructor CreateFmt( const Msg : string; const Args : array of const ); reintroduce;

	end;

implementation

const
	CursesArray : array [ 0 .. 186 ] of string = ( 'Anf�kta och anamma', 'Anamma och regera alla v�rldens andemakter',
		'Amiral Nelsons alla bomber och granater och kanoner', 'Am�bor', 'Apsvansade analfabeter', 'Apspektakel', 'Asgamar',
		'Avskyv�rt', 'Babian', 'Bandit', 'Banditskojaren', 'Bestar', 'Bladlus', 'Blindstyren', 'Blixt, brak och dunder',
		'Blodsugare', 'Bl�kullatomtar', 'Bl�tdjur', 'Bomber och granater och krevader', 'Bondlurkar', 'Bovar', 'B�ln�tter',
		'B�del', 'Degh�gar', 'Det var som topp tusen tunnor', 'Drummel', 'Dr�nare', 'Dunder och brak',
		'Du �r en pesto...klyster', 'Dyngspridare', 'D�rhushjon', 'D�d och pina', 'El�ndiga kryp', 'En�gda kannibal',
		'Erbarmliga plattf�tter', 'Far och flyg', 'Fega ynkryggar', 'F�ntrattar', 'F�hund', 'F�rd�mda kr�k',
		'F�rpiskade luspudlar', 'F�r hundra gubbar', 'F�r sjutton hakar', 'Gamla kn�lsvan', 'Gangster', 'Gargantuaner',
		'Giftbl�san', 'Gnom', 'Gr�suggor och spindelapor', 'Gravade oxsvansar', 'Grobianer', 'Groteskt', 'Grottm�nniskor',
		'Gr�suggor och dreglande paddor', 'Gr�sligt', 'Gyckel, b�g och ordkonster', 'Hamstrare', 'Huggormars avf�da',
		'Idiot', 'Injs�gangster', 'I alla milda makters namn', 'J�klar', 'J�kla general', 'J�kla odjur',
		'J�mmer och el�nde', 'Kanalier', 'Kanap�er och konjakskransar', 'Kannibal', 'Karnevalsdiktatorn', 'Kloakdjur',
		'Kloakr�tta', 'Klorlutsoppan', 'Kramsf�gelm�rdare', 'Krevader och kanoner', 'Kryddkr�mare', 'Kr�mare',
		'Legodr�ngar', 'Lekstugefasoner', 'Lumphandlare', 'Luspudlar', 'Luskungar', 'Lymmel', 'L�gnhalsar', 'L�nnm�rdare',
		'Maskar', 'Milda makter', 'Murmeldjur', 'M�shumlor', 'Nu blommar asfalten', 'Nu g�r skam p� torra land', 'Ockrare',
		'Oduglingar', 'Orangutang', 'Parasiter', 'Pestr�tta', 'Pest och pina', 'Pillerbagge', 'Pirater',
		'Pistaschgubbar och surk�lsstuvning', 'Plattf�tter', 'Potatisgrisar', 'Pottsorkar', 'Pyromanapa', 'Rena r�vgiftet',
		'Rena sammansv�rjningen', 'Rena snurren', 'Rena vansinnet', 'Rotborstar', 'Rullsvansapa', 'R�ttsvansar',
		'R�dskinnen', 'R�t�gg', 'Sakramentskade sumprunkare', 'Sabot�rer och pestr�ttor', 'Sladderf�gel', 'Sillmj�lkar',
		'Skabbhalsar', 'Skabbr�ttor', 'Skamligt', 'Skojare', 'Skottkolvar', 'Skurkar', 'Skunkdjur', 'Slavhandlare',
		'Slyn-yngel', 'Snack och strunt och snack', 'Snorvalp', 'Snyltror p� �del tistelstam', 'Soppr�tter', 'Sumpr�tta',
		'S�tvattenspirater', 'Tusan', 'Tjockskalle', 'Tjurskalle', 'Tjuvar', 'Tryffelsvin', 'T�ngr�ka', 'Urf�nigt',
		'Uslingar', 'Vagabonder', 'Vandaler', 'Vegetarian', 'Vidriga apm�nniska', 'Vidriga varulvar', 'Vinlus',
		'Vrakplundrare', 'Vr�lapor', 'V�rtsvin', '�sneskallar', '�rkebanditer', '�rkeboven', '�rkel�gnare',
		'An�da och anagga', 'Avsigkomne', 'Blixt och dunder och miljoner tyfoner', 'Blixt och skatter', 'Bl�ckfiskar',
		'Din olycka', 'Din skr�puk', 'Ditt bj�bbande sp�ke', 'Ditt en�dga murmeldjur', 'Era tjattrande kariblar',
		'F�r b�velen', 'F�rdubblade babianer', 'F�rdubblade galenskaper', 'F�r h�ge farao', 'Gorillor',
		'Iglar och blodsugare', 'Jag ska g�ra papegojpaj av hela rasket', 'Kn�lf�tter', 'Kornblixtar och tyfoner',
		'Kors i v�rlden', 'Kreatur', 'Lomh�rda lymmel', 'Maneter', 'Markattor', 'Monster och morsgrisar', 'Passg�ngarna',
		'Pottsorkar och palsternackor', 'Sj�gurkor', 'Sm�fjantar', 'Stend�va stallknekt', 'Stinkbomben', 'Tokskallar',
		'Vederv�rdige', 'Vid alla nerlusade soppr�tter', 'Vid alla piskande r�ttsvansar' );

	{ EHaddockException }

function EHaddockException.CaptianHaddockSays : string;
begin

	Result := CursesArray[ Random( 186 ) ];

end;

constructor EHaddockException.Create( const Msg : string );
var
	TheCap : string;
begin

	Randomize;

	TheCap := CursesArray[ Random( 186 ) ];
	inherited Create( TheCap + '!!!' + #13#10#13#10 + 'Det f�rd�mda programmet svarade: ' + Msg );

end;

constructor EHaddockException.CreateFmt( const Msg : string;
	const Args : array of const );
var
	TheCap : string;
begin

	TheCap := CursesArray[ Random( 186 ) ];
	inherited CreateFmt( TheCap + '!!!' + #13#10#13#10 + 'Det f�rd�mda programmet svarade: ' + Msg, Args );

end;

end.
